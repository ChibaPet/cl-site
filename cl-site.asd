;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site
  :name "cl-site"
  :version "0.0.1"
  :maintainer "Common-Lisp.Net maintainers"
  :author "C. Yang & Common-Lisp.Net maintainers"
  :licence "TBD"
  :description "Static site generator for common-lisp.net, written in CL"
  :depends-on (:cl-mustache :plump :cl-markdown :closer-mop :cl-who :cl-yaml)
  :components ((:module source
                        :pathname ""
                        :serial t
                        :depends-on (package)
                        :components ((:file "patches")
                                     (:file "globals")
                                     (:file "helpers")
                                     (:file "process")
                                     (:file "main")))
               (:module package
                        :pathname ""
                        :components ((:file "package"))))
  :in-order-to ((asdf:test-op (asdf:test-op cl-site/t))))

;;(asdf:defsystem cl-site/t
;;  :defsystem-depends-on (prove-asdf)
;;  :depends-on (cl-site prove)
;;  :components ((:module site
;;                        :pathname "t/"
;;                        :components ((:test-file "site"))))
;;  :perform (asdf:test-op (op c)
;;                         (uiop:symbol-call :prove-asdf 'run-test-system c)))
