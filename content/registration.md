title: How to register on Gitlab.common-lisp.net

---

# Registration on gitlab.c-l.net made harder

Due to the large amount of users that try to register for spam purposes we've 
had to make registration (*and login via Google*) a bit harder.

## One step more

Please message `minion` on `#common-lisp.net` on Libera IRC about
the registration URL; you can either use your own IRC client, or use
[webchat](https://web.libera.chat)
need to "Auth to services").

The right question would be the string `minion: registration, please?`; you'll 
receive answer in near-realtime.

The URL you receive can be used both for registration, as well as for logging in 
via Google; if you already have a local user or use GitHub, these restrictions 
do not (yet) apply.

